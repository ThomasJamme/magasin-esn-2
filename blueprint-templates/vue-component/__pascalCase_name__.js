import {{pascalCase name}}I18n from './{{pascalCase name}}.i18n.js';

export default {

  components: {},

  props: {},

  computed: {},

  data() {
    return {};
  },

  watch: {},

  methods: {},

  mounted() {},

  i18n: {{pascalCase name}}I18n,

};
