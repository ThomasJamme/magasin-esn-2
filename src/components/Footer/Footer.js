import FooterI18n from './Footer.i18n.js';

export default {

  components: {},

  props: {},

  data() {
    return {};
  },

  watch: {},

  methods: {
    goToThomasSite() {
      window.open('https://portefoliothomasdelagoutine.firebaseapp.com/', '_blank');
    }
  },

  mounted() {},

  i18n: FooterI18n,

};
