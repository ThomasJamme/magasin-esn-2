import NavI18n from './Nav.i18n.js';

export default {

  components: {},

  props: {
    categories: Array
  },

  data() {
    return {};
  },

  watch: {},

  methods: {
    goToCat(pCat) {
      const catId = pCat.id
      this.$router.push({ path: `/categorie/${catId}` })
    },
    goToContact() {
      this.$router.push({ path: `/contact` })
    }
  },

  mounted() {},

  i18n: NavI18n,

};
