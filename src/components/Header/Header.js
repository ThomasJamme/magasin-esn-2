import HeaderI18n from './Header.i18n.js';
import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'
export default {

  components: {},
  computed: {
    ...mapGetters(['cart'])
  },
  props: {},

  data() {
    return {};
  },

  watch: {},

  methods: {
    /**
     * Get the number of cart item.
     */
    getCartLength() {
      return this.cart.length
    },
    /**
     * Function to go to cart page.
     */
    goToCart() {
      this.$router.push({
        path: `/panier`
      })
    },
    /**
     * Function to go home page.
     */
    goToHome() {
      this.$router.push({
        path: `/`
      })
    }

  },

  mounted() {},

  i18n: HeaderI18n,

};
