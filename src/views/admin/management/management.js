import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'
export default {
  data() {
    return {
      currentDisplay: 'cat',
      // Cat
      currentCategoryName: '',
      currentCategoryOrder: 1,
      currentCategoryPhoto: '',
      currentCategoryId: 0,
      // Product
      currentProductId: '',
      currentProductName: '',
      currentProductRef: '',
      currentProductPrice: 0,
      currentProductStock: '',
      currentProductDesc: '',
      productParentCat: null,
      fileProduct: null,
      fileProduct2: null,
      fileProduct3: null,
      imageProduct: '',
      imageProduct2: '',
      imageProduct3: '',
      currentProductPhoto: '',
      // Order
      currentOrder: null,
      // Fact
      totalOrders: 0,
      // General
      creationMode: 'create',
    }
  },
  computed: {
    ...mapGetters([
      'categories', 'products', 'dataLoading', 'dataUploading', 'orders', 'payed'
    ])
  },
  methods: {
    ...mapActions([
      'addCategory', 'getCategories', 'deleteCategory', 'addProduct', 'getProducts',
      'deleteProduct', 'getOrders', 'validateOrder', 'getOrdersDone', 'deleteOrder', 'getPayed', 'updateCategory', 'updateProduct', 'uploadImage'
    ]),

    uploadFile(event) {
      this.imageProduct = event.target.file
      console.log(this.imageProduct)
    },


    /**
     * Function to manange categories
     */
    createCategory() {
      if (this.creationMode === 'create') {
        const category = {
          name: this.currentCategoryName,
          order: this.currentCategoryOrder,
          photo: this.currentCategoryPhoto
        }
        this.addCategory(category).then(res => {
          console.log(res)
          this.getCategories()
          this.hideModalAddCategory()
        }).catch(err => {
          console.log(err)
        })
      } else {
        const category = {
          id: this.currentCategoryId,
          name: this.currentCategoryName,
          order: this.currentCategoryOrder,
          photo: this.currentCategoryPhoto
        }
        this.updateCategory(category).then(res => {
          console.log(res)
          this.getCategories()
          this.hideModalAddCategory()
        }).catch(err => {
          console.log(err)
        })
      }
    },
    removeCategory() {
      const category = {
        id: this.currentCategoryId,
      }
      this.deleteCategory(category).then(res => {
        console.log(res)
        this.getCategories()
        this.hideModalDelete()
      }).catch(err => {
        console.log(err)
      })
    },
    showModalUpdateCategory(pCategoryId, pCategoryName, pCategoryOrder, pPhoto) {
      this.currentCategoryId = pCategoryId
      this.currentCategoryName = pCategoryName
      this.currentCategoryOrder = pCategoryOrder
      this.currentCategoryPhoto = pPhoto
      this.creationMode = 'update'
      this.$refs['modal-add-category'].show()
    },

    /**
     * Function to manage products
     */
    createProduct() {
      if (this.creationMode === 'create') {

        const product = {
          name: this.currentProductName,
          stock: this.currentProductStock,
          desc: this.currentProductDesc,
          price: this.currentProductPrice,
          photo: this.currentProductPhoto,
          categoryId: this.productParentCat.id,
          categoryName: this.productParentCat.name,
        }
        this.addProduct(product).then(res => {
          this.currentProductId = ''
          this.currentProductName = ''
          this.currentProductRef = ''
          this.currentProductPhoto = ''
          this.currentProductPrice = 0
          this.currentProductStock = ''
          this.currentProductDesc = ''
          // let formData = new FormData()
          // formData.append('file', this.fileProduct)
          // const info = {
          //   id: res.data._id,
          //   file: formData
          // }
          // this.uploadImage(info).then(res => {
          //   this.getProducts()
          //   this.hideModalAddProduct()
          // }).catch(err => {
          //   console.log(err)
          // })
         
        }).catch(err => {
          console.log(err)
        })
      } else {
        const product = {
          id: this.currentProductId,
          name: this.currentProductName,
          stock: this.currentProductStock,
          desc: this.currentProductDesc,
          price: this.currentProductPrice,
          photo: this.currentProductPhoto,
          parentId: this.productParentCat.id,
          parentName: this.productParentCat.name,
        }
        this.updateProduct(product).then(res => {
          this.currentProductId = ''
          this.currentProductName = ''
          this.currentProductRef = ''
          this.currentProductPhoto = ''
          this.currentProductPrice = 0
          this.currentProductStock = ''
          this.currentProductDesc = ''
          this.getProducts()
          this.hideModalAddProduct()
        }).catch(err => {
          console.log(err)
        })
      }
    },
    onFileChangeProduct(e) {
      var files = e.target.files || e.dataTransfer.files
      if (!files.length) {
        return
      }
      this.createImageProduct(files[0])
    },
    onFileChangeProduct2(e) {
      var files = e.target.files || e.dataTransfer.files
      if (!files.length) {
        return
      }
      this.createImageProduct2(files[0])
    },
    onFileChangeProduct3(e) {
      var files = e.target.files || e.dataTransfer.files
      if (!files.length) {
        return
      }
      this.createImageProduct3(files[0])
    },
    createImageProduct(file) {
      this.fileProduct = file
      const image = new Image()
      const reader = new FileReader()
      const vm = this

      reader.onload = (e) => {
        vm.imageProduct = e.target.result
      }
      reader.readAsDataURL(file)
      this.needImageUpdate = true
    },
    removeImageProduct(e) {
      this.imageProduct = ''
    },
    createImageProduct2(file) {
      this.fileProduct2 = file
      const reader = new FileReader()
      const vm = this

      reader.onload = (e) => {
        vm.imageProduct2 = e.target.result
      }
      reader.readAsDataURL(file)
      this.needImageUpdate = true
    },
    removeImageProduct2(e) {
      this.imageProduct2 = ''
    },
    createImageProduct3(file) {
      this.fileProduct3 = file
      const reader = new FileReader()
      const vm = this

      reader.onload = (e) => {
        vm.imageProduct3 = e.target.result
      }
      reader.readAsDataURL(file)
      this.needImageUpdate = true
    },
    removeImageProduct3(e) {
      this.imageProduct3 = ''
    },
    showModalUpdateProduct(pProductId, pProductName, pProductPrice, pProductStock, pProductDesc, pProductParentId, pProductImageUrl) {
      this.currentProductId = pProductId
      this.currentProductName = pProductName
      this.currentProductStock = pProductStock
      this.currentProductDesc = pProductDesc
      this.currentProductPrice = pProductPrice
      this.categories.forEach(cat => {
        if (cat.id === pProductParentId) {
          this.productParentCat = cat
        }
      });
      this.currentProductPhoto = pProductImageUrl
      this.creationMode = 'update'
      this.$refs['modal-add-product'].show()
    },
    removeProduct() {
      const product = {
        id: this.currentProductId,
      }
      this.deleteProduct(product).then(res => {
        console.log(res)
        this.currentProductId = ''
        this.getProducts()
        this.hideModalDelete()
      }).catch(err => {
        console.log(err)
      })
    },

    // Order
    getTotalOrder(pCart) {
      let total = 0
      pCart.forEach(aItem => {
        total = parseFloat(aItem.totalProductPrice + total).toFixed(2)
      })
      return total
    },
    loadOrderDone() {
      this.getOrdersDone()
    },
    loadOrder() {
      this.getOrders()
    },
    openValidationOrder(pOrder) {
      this.currentOrder = pOrder
      this.$refs['validate-order'].show()
    },
    openDeleteOrder(pOrder) {
      this.currentOrder = pOrder
      this.$refs['delete-order'].show()
    },
    validateCurrentOrder() {
      this.validateOrder(this.currentOrder)
      this.$refs['validate-order'].hide()
      this.loadOrder()
    },
    deleteCurrentOrder() {
      this.deleteOrder(this.currentOrder).then(res => {
        this.loadOrder()
        this.$refs['delete-order'].hide()
      })
     
      
    },
    hideModalValidateOrder() {
      this.$refs['validate-order'].hide()
    },
    hideModalDeleteOrder() {
      this.$refs['delete-order'].hide()
    },

    uploadCurrentImage() {
    
    },

    // Facturation
    getTotalOrders() {
      this.totalOrders = 0
      this.orders.forEach(aOrder => {
        if (aOrder.valid) {
          aOrder.cart.forEach(aCart => {
            this.totalOrders = parseFloat(aCart.totalProductPrice) + parseFloat(this.totalOrders)
          })
        }
      })
    },

    getFraisGestion() {
      return (this.totalOrders * 0.01).toFixed(2)
    },


    // General
    displaySection(pCurrentDisplay) {
      this.currentDisplay = pCurrentDisplay
      if (pCurrentDisplay === 'cmd') {
        this.loadOrder()
      }
      if (pCurrentDisplay === 'arc') {
        this.loadOrderDone()
      }
      if (pCurrentDisplay === 'cat') {
        this.getCategories()
      }
      if (pCurrentDisplay === 'prod') {
        this.getProducts()
        this.creationMode = "create"
      }
      if (pCurrentDisplay === 'fac') {
        this.getOrdersDone()
        this.getTotalOrders()
        this.getPayed()
      }

    },
    remove() {
      switch (this.currentDisplay) {
        case 'cat':
          this.removeCategory()
          break
        case 'prod':
          this.removeProduct()
      }
    },
    // Modals
    showModalAdd() {
      switch (this.currentDisplay) {
        case 'cat':
          this.showModalAddCategory()
          break
        case 'prod':
          this.showModalAddProduct()
      }
    },
    showModalAddCategory() {
      this.currentCategoryId = ''
      this.currentCategoryName = ''
      this.currentCategoryOrder = 1
      this.currentCategoryPhoto = ''
      this.creationMode = 'create'
      this.$refs['modal-add-category'].show()
    },
    hideModalAddCategory() {
      this.$refs['modal-add-category'].hide()
    },
    showModalAddProduct() {
      this.currentProducId = ''
      this.currentProductName = ''
      this.currentProducRef = ''
      this.currentProducPrice = 0
      this.currentProductStock = ''
      this.currentProductPhoto = ''
      this.currentProductDesc = ''
      this.productParentCat = null
      this.fileProduct = null
      this.imageProduct = ''
      this.creationMode = 'create'
      this.$refs['modal-add-product'].show()
    },
    hideModalAddProduct() {
      this.$refs['modal-add-product'].hide()
    },
    showModalDelete(pId) {
      switch (this.currentDisplay) {
        case 'cat':
          this.currentCategoryId = pId
          break
        case 'prod':
          this.currentProductId = pId
          break
      }
      this.$refs['delete-confirmation'].show()
    },
    hideModalDelete() {
      this.$refs['delete-confirmation'].hide()
    },
    goToStore() {
      this.$router.push('/')
    }
  },
  mounted() {
    this.getCategories()
    this.getProducts()
    this.getOrders()
  },

  filters: {
    formatDate: function (value) {
      if (!value) return ''
      const date = new Date(value)
      const options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      }
      return date.toLocaleDateString('fr-FR', options)
    },
    toFirstUpperCase: function (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    }
  }
}