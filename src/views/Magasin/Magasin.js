import MagasinI18n from './Magasin.i18n.js';
import pHeader from '@/components/Header/Header.vue'
import pFooter from '@/components/Footer/Footer.vue'
import pNav from '@/components/Nav/Nav.vue'
import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'

export default {

  components: {pHeader, pFooter, pNav},

  props: {},

  data() {
    return {};
  },
  computed: {
    ...mapGetters(['categories'])
  },

  watch: {},

  methods: {
    ...mapActions(['getCategories'])
  },

  mounted() {
    this.getCategories().then(res => {

    }).catch(err => {
      console.log(err)
    })
  },

  i18n: MagasinI18n,

};
