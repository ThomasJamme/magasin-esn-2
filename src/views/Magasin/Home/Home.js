import HomeI18n from './Home.i18n.js';

export default {

  components: {},

  props: {},

  data() {
    return {};
  },

  watch: {},

  methods: {},

  mounted() {},

  i18n: HomeI18n,

};
