import PanierI18n from './Panier.i18n.js';
import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'
export default {

  components: {},
  computed: {
    ...mapGetters(['cart'])
  },
  props: {},

  data() {
    return {
      total: 0
    };
  },

  watch: {},

  methods: {
    ...mapMutations(['deleteFromCart', 'setTotalProductPrice']),
    /**
     * Remove item from cart.
     * @param {*} pProduct 
     */
    revoveProduct(pProduct) {
      this.deleteFromCart(pProduct)
    },
    /**
     * Set the price of the product line.
     * @param {*} pQuantity 
     * @param {*} pPrice 
     */
    getTotal() {
      this.total = 0
      this.cart.forEach(item => {
        this.total = this.total + parseFloat(item.totalProductPrice)
      })
    },
    /**
     * Update the price of product line.
     * @param {*} pProduct 
     */
    setProductPriceLine(pProduct) {
      this.setTotalProductPrice(pProduct)
      this.getTotal()
      this.$forceUpdate()
    },
    goToOrder() {
      this.$router.push({ path: `/commande` })
    }
  },

  mounted() {
    this.getTotal()
  },

  i18n: PanierI18n,

};
