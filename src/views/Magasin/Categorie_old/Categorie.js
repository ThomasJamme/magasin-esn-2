import CategorieI18n from './Categorie.i18n.js'
import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'

export default {

  components: {},
  computed: {
    ...mapGetters(['families', 'categories', 'dataLoading']),
    /**
     *  Computed to get he category name.
     */
    getCategoryName() {
      let category
      if (this.categories.length > 0) {
        this.categories.forEach(aCat => {
          if (aCat.id === this.$route.params.catId) {
            category = aCat
          }
        })
        return category.name
      } else {
        return ""
      }
    }
  },

  props: {},

  data() {
    return {};
  },

  watch: {
    '$route'(to, from) {
      this.loadFamilies()
    }
  },

  methods: {
    ...mapActions(['getFamiliesByCategory']),

    /**
     * Load families.
     */
    loadFamilies() {
      this.getFamiliesByCategory(this.$route.params.catId).then(res => {
      }).catch(err => {
        console.log(err)
      })
    },
  },

  mounted() {
    this.loadFamilies()

  },

  i18n: CategorieI18n,

};
