import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'
export default {

  components: {},
  computed: {
    ...mapGetters(['categories', 'dataLoading', 'products', 'cart']),
    /**
     *  Computed to get he category name.
     */
    getCategoryName() {
      let category
      if (this.categories.length > 0) {
        this.categories.forEach(aCat => {
          if (aCat.id === this.$route.params.catId) {
            category = aCat
          }
        })
        return category.name
      } else {
        return ""
      }
    },
  },
  props: {},

  data() {
    return {
      currentProduct: null,
      currentProductQuantity: 1,
    };
  },

  watch: {
    '$route'(to, from) {
      this.loadProducts()
    }
  },

  methods: {
    ...mapMutations(['addToCart']),
    ...mapActions(['getCategories', 'getProductsByCategory']),
    /**
     * Load products
     */
    loadProducts() {
      this.getProductsByCategory(this.$route.params.catId).then(res => {
        console.log('loaded')
      }).catch(err => {
        console.log(err)
      })
    },
    addProduct(pProduct) {
      this.currentProduct = pProduct
      this.$refs['modal-quantity'].show()
    },
    upQuantity() {
      let step = 1
      if (this.currentProduct.unit === 'kg') {
        step = 0.1
      }
      let value = this.currentProductQuantity + step
      this.currentProductQuantity = parseFloat(value.toFixed(2))
    },
    downQuantity() {
      let step = 1
      if (this.currentProduct.unit === 'kg') {
        step = 0.1
      }
      let value = this.currentProductQuantity - step
      if (value > 0) {
        this.currentProductQuantity = parseFloat(value.toFixed(2))
      }
    },
    /**
     * Add to cart.
     */
    addProductToCart() {
      this.currentProduct.quantity = this.currentProductQuantity
      this.currentProduct.totalProductPrice = parseFloat(this.currentProduct.quantity * parseFloat(this.currentProduct.price)).toFixed(2)
      this.addToCart(this.currentProduct)
      this.$refs['modal-quantity'].hide()
    },
  },

  mounted() {
    this.loadProducts()
  },


};