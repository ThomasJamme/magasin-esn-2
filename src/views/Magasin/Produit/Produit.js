import ProduitI18n from './Produit.i18n.js';
import {
  mapGetters,
  mapActions,
  mapMutations,
} from 'vuex'
export default {

  components: {},

  computed: {
    ...mapGetters(['families', 'dataLoading', 'products', 'cart']),
    /**
     *  Computed to get the category name.
     */
    product() {
      let product
      if (this.products.length > 0) {
        this.products.forEach(aProd => {
          if (aProd.id === this.$route.params.prodId) {
            product = aProd
          }
        })
        return product
      } else {
        return ""
      }
    },
  },

  props: {},

  data() {
    return {
      currentPhotoLink: ''
    };
  },

  watch: {},

  methods: {
    ...mapMutations(['addToCart']),
    /**
     * Function to change the current photo.
     * @param {*} pPhotoLink 
     */
    changePhoto(pPhotoLink) {
      this.currentPhotoLink = pPhotoLink
    },
     /**
     * Function to know if the product is already in the cart.
     * @param {*} pProd 
     */
    isInCart(pProd) {
      let isInCart = false
      this.cart.forEach(aProduct => {
        if (aProduct.id === pProd.id) {
          isInCart = true
        }
      })
      return isInCart
    },

    /**
     * Add to cart.
     */
    addProductToCart(pProduct) {
      pProduct.quantity = 1
      pProduct.totalProductPrice = pProduct.price
      this.addToCart(pProduct)
    },
  },

  mounted() {
    this.currentPhotoLink = this.product.photo
  },

  i18n: ProduitI18n,

};
